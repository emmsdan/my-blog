import React, { useEffect, useState } from 'react';
import { Link } from 'react-router-dom';
import { getImageColor } from '../modules/image';
// import { getVideo } from '../services/blog';

const randomNum = (num=20) => `https://picsum.photos/id/${Math.floor(Math.random() * num)}/300/200`;

export default function Card(props) {
 const [styles, setStyles ] = useState({ color: '#fff', isLoading: true })
 // console.log(getVideo("7lCDEYXw3mM"))
 const image = randomNum(100);
 if (styles.isLoading) {
   getImageColor(image).then(res => setStyles({ background: res}))
 }
 return (<> 
 <div className="card">
  <img src={image} />
  <div className="overlay">
   <button className="btn circle border-0" style={styles}><i className="material-icons">more_horiz</i></button>
   <h5>view title goes here, and it's very very very long oh.</h5>
  </div>
 </div>
 </>)
 }