import React from 'react';
import logo from './logo.svg';
import './App.css';
import Card from './components/Card';

function App() {
  return (
    <div className="App">
      <header className="App-header p-3">
        <img src={logo} className="App-logo" alt="logo" /> 
         <Card />
         <Card />
         <Card />
         <Card />

      <br />
      <br />
      <br />
      </header>
      <br />
      <br />
      <br />
      <br />
    </div>
  );
}

export default App;
